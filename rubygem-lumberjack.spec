# Generated from lumberjack-1.0.12.gem by gem2rpm -*- rpm-spec -*-
%global gem_name lumberjack

Name: rubygem-%{gem_name}
Version: 1.0.12
Release: 1%{?dist}
Summary: A simple, powerful, and very fast logging utility
License: MIT
URL: http://github.com/bdurand/lumberjack
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby
BuildRequires: rubygem(rspec)
BuildRequires: rubygem(timecop)
BuildArch: noarch

%description
A simple, powerful, and very fast logging utility that can be a drop in
replacement for Logger or ActiveSupport::BufferedLogger. Provides support for
automatically rolling log files even with multiple processes writing the same
log file.


%package doc
Summary: Documentation for %{name}
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/



%check
pushd .%{gem_instdir}
rspec spec
popd

%files
%dir %{gem_instdir}
%license %{gem_instdir}/MIT_LICENSE
%{gem_instdir}/VERSION
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/README.md
%{gem_instdir}/Rakefile
%{gem_instdir}/spec

%changelog
* Thu Dec 07 2017 Jaroslav Prokop <jar.prokop@volny.cz> - 1.0.12-1
- Update to newest version

* Wed May 08 2013 Anuj More - 1.0.3-2
- Moving MIT_LICENSE to the main package and removing the redundant BuildRequires: rubygem(rspec-core)

* Sun May 05 2013 Anuj More - 1.0.3-1
- Initial packag
